#!/bin/bash

function w3c-validation() {
  # Check arguments
  local paths=()
  while [ $# -gt 0 ]
  do
    if [[ $1 == "-h" || $1 == "--help" ]]
    then
      local program_name=${FUNCNAME[0]}
      echo 'Check W3C rules for all ".html" files found.'
      echo ''
      echo "Usage: ${program_name} [<file>]*"
      return 0
    else
      paths+=($1)
    fi
    shift
  done

  local url="https://validator.w3.org/nu/"

  # Check API status
  local status=$(curl -s ${url} \
    --write-out '%{http_code}' \
    --output /dev/null)
  if [[ ${status} -ne "200" ]]
  then
    echo "ERROR: response status code is ${status}"
    return 1
  fi

  # Process each files
  for path in "${paths[@]}"
  do
    if [[ -d ${path} || -f ${path} ]]
    then
      # Process each file of this folder
      for file in $(grep "" --recursive ${path} --include \*.html --files-with-matches)
      do
        # Check this file
        echo "Checking file ${file}..."

        local errors=$(curl -s ${url}?out=json \
          --header "Content-Type: text/html" \
          --data-binary @${file} \
        | jq -r ".messages")

        if [[ ${errors} != [] ]]
        then
          echo ${errors} | jq .
          local has_errors=true
        fi
      done
    else
      echo "Invalid path \"${path}\" ignored"
    fi
  done
  
  if [[ ! -z "${has_errors}" ]]
  then
    return 1
  fi
}
