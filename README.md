# W3C - HTML Validator

Bash script to validate HTML files according to the W3C rules.

## Dependencies

The script uses [`jq`](https://stedolan.github.io/jq/download/) package to communicate with the [W3C API](http://validator.w3.org/docs/api.html).

## Validate HTML files

To validate one or more HTML files, run the following command:

```
source validator.sh && w3c-validation [<file>]*
```

This command will validate each `.html` file found and display their errors.
